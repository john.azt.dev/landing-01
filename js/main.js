const createForLoop = (container, limit) => {
  for (let i = 1; i <= limit; i++) {
    container.push(i)
  }

  return container
}

const array = []

const myNewArray = createForLoop(array, 7)

const newArrayManipulation = () => {
  return myNewArray.filter(val => val < 4);
}

console.log(newArrayManipulation());

